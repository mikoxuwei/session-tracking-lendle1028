<%-- 
    Document   : cookie
    Created on : Sep 13, 2018, 9:19:51 PM
    Author     : lendle
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            String food="";
            Cookie [] cookies=request.getCookies(); //Servlet 通過 request.getCookies() 訪問 Cookie，將返回一個 Cookie 對象的數組。
//            會讀取全部cookie，讀取量會很大。
            for(Cookie cookie : cookies){
                if(cookie.getName().equals("food")){
                    food = URLDecoder.decode(cookie.getValue(), "utf-8"); //一定成雙成對，意思是serve和前端都要有 , 解碼。
                    break;
                }
            }
        %>
        <form action="saveCookie" method="POST">
            Your Favorite Food: <input type="text" name="food" value="<%=food%>"/><br/>
            <input type="submit"/>
        </form>
    </body>
</html>
