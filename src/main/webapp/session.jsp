<%-- 
    Document   : cookie
    Created on : Sep 13, 2018, 9:19:51 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            String food=(String)session.getAttribute("food"); // 返回session對像中與指定名稱綁定的對象，如果不存在則返回null
            if(food==null){
                food="";
            }
        %>
        <form action="<%=response.encodeURL("saveSession")%>" method="POST"> <!--action="saveSession" , 編碼--> 
            Your Favorite Food: <input type="text" name="food" value="<%=food%>"/><br/>
            <input type="submit"/>
        </form>
    </body>
    <!--HTTP是無狀態協議，這意味著每次客戶端檢索網頁時，都要單獨打開一個服務器連接，因此服務器不會記錄下先前客戶端請求的任何信息。-->
    <!--網絡服務器可以指定一個唯一的 session ID 作為 cookie 來代表每個客戶端，用來識別這個客戶端接下來的請求。-->
</html>
